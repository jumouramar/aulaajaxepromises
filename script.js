// FETCH 
/*
const fetchConfig = {
    "method": "GET"
}

fetch("https://treinamento-ajax.up.railway.app/api/v1/messages", fetchConfig)
    .then((resposta)=>{      // aciona logo assim que a promise for concluida 
        resposta.json()
            .then((resposta)=>{
                console.log(resposta)
            })
            .catch((error)=>{
                console.log(error)
            })
    }) 
    .catch((error)=>{        // aciona caso a promise for rejeitada
        console.log(error)
    })
*/

const urlApi = "https://treinamento-ajax.up.railway.app/api/v1/messages"
const section_mensage = document.querySelector(".section_mensage");

function createCard(author, content){
    let card = document.createElement("div")
    let authorP = document.createElement("p")
    let contentP = document.createElement("p")

    card.classList.add("mensage")
    contentP.classList.add("content")
    authorP.classList.add("author")

    contentP.innerText = content
    authorP.innerText = author

    card.append(contentP)
    card.append(authorP)

    section_mensage.append(card)
}

// GET

function getMensages(){

    section_mensage.innerHTML = "";

    const fetchConfig = {
        "method": "GET"
    }
    
    fetch(urlApi, fetchConfig)
        .then((resposta)=>{
            resposta.json()
                .then((resposta)=>{
                    resposta.map((mensage)=>{
                        createCard(mensage.author, mensage.content);
                    })
                })
                .catch((error)=>{
                    console.log("error 2")
                    console.log(error)
                })
        })
        .catch((error)=>{
            console.log("error 1")
            console.log(error)
        })
}

// POST

async function postMensage(){
    let author = document.querySelector(".input_author").value;
    let content = document.querySelector(".input_content").value;

    let fetchBody = {
        "author": author,
        "content": content
    }

    const fetchConfig = {
        "method": "POST",
        "body": JSON.stringify(fetchBody),
        "headers": {"Content-Type" : "application/json"}
    }

    await fetch(urlApi, fetchConfig)
        .then((resposta)=>{
            resposta.json()
                .then((resposta)=>{
                    console.log(resposta);
                })
                .catch((error)=>{
                    console.log(error)
                })
        })
        .catch((error)=>{
            console.log(error)
        })

    getMensages()

}

let button_send = document.querySelector(".btn_send");
button_send.addEventListener("click",()=>{postMensage()})

getMensages()