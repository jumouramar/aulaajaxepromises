// isso é um objeto de configuração
const fetchConfig = {
    "method": "GET"
}

// fetch tem dois parâmetros
fetch("urlRotaDaApi", objetoDeConfiguração)
// objeto de configuração retorna uma promise
// duas funções para tratar a promise
    // se for concluída
    .then((resposta) => {
        console.log(resposta)
    })
    // se for rejeitada
    .catch((error) => {
        console.log(error)
    })


// a resposta é retornada em JSON e precisamos transformar em JS
// função .json()
resposta.json()

/////////////////////////////////////////////////////////////////////////////////

// no caso do post, é necessário um body e um header na configuração
// criando o corpo do objeto
let fetchBody = {
    "exemplo1": exemplo1,
    "exemplo2": exemplo2
} 
// objeto de configuração
const fetchConfigPost = {
    "method": "POST", 
    "body": JSON.stringify(fetchBody), // precisamos transformar em JSON
    "headers": {"content-Type": "application/json"} // precisa avisar que é um JSON
}