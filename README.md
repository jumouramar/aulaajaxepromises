# Anotações
### HTTP
Protocolo que permite obter de recursos.
Requisições HTTP são mensagens enviadas pelo cliente para iniciar uma ação no servidor.
5 métodos descrevem qual ação será executada:

GET - solicita um dado

POST - insere um dado

PUT - edita um elemento

PATCH - edita um campo específico

DELETE - deletar um dado

---

### Insomnia
Ferramenta para testar as rotas e requisições

### AJAX
Método que utiliza js e xml para conseguir fazer as requisições de forma assíncrona 

### Promise
Objeto que representa um evento/processo de uma função assíncrona

### Fetch
Ferramenta para fazer a requisição